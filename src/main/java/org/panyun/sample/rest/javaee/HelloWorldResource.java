/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.panyun.sample.rest.javaee;

import com.google.gson.Gson;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Pan Yun
 */
@Path("helloworld")
public class HelloWorldResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of HelloWorldResource
     */
    public HelloWorldResource() {
    }

    /**
     * Retrieves representation of an instance of org.panyun.sample.rest.javaee.HelloWorldResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        JsonObject jsonObj = Json.createObjectBuilder()
                .add("val", "Hello World!")
                .build();
        Response response = Response.ok(jsonObj).build();
        return response;
    }

    /**
     * PUT method for updating or creating an instance of HelloWorldResource
     * @param content representation for the resource
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(String content) {
        Response response = Response.status(Response.Status.PRECONDITION_FAILED).build();
        Gson gson = new Gson();
        UserDto userDto = gson.fromJson(content, UserDto.class);
        response = Response.ok().build();
        return response;
    }
}
